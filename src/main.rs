use actix::prelude::*;

// this is our Message
#[derive(Message)]
#[rtype(result = "String")] // we have to define the response type for `Sum` message
struct GreetingMessage(String, String);

// Actor definition
struct Greet;

impl Actor for Greet {
    type Context = Context<Self>;
}

// now we need to define `MessageHandler` for the `Sum` message.
impl Handler<GreetingMessage> for Greet {
    type Result = String; // <- Message response type

    fn handle(&mut self, msg: GreetingMessage, ctx: &mut Context<Self>) -> Self::Result {
        format!("{} {}", msg.1, msg.0)
    }
}

#[actix_rt::main] // <- starts the system and block until future resolves
async fn main() {
    let greet = Greet.start();
    let res = greet.send( GreetingMessage("Daniel".to_string(), "Hello".to_string() )).await; // <- send message and get future for result

    match res {
        Ok(result) => println!("{}", result),
        _ => println!("Communication to the actor has failed"),
    }
}